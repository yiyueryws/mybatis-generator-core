### 基于Mybatis-generator-core进行二次开发。



由于这个生成框架是外国人写的，多多少少有些地方看着不习惯。所以进行二次开发根据以往项目经验修改一个目前主流开发层级设计（Controller、Service、DAO）生成框架。



### 新开发功能：

1.对mapper.xml内容进行优化。   
2.生成Service层代码。   
3.生成Controller层代码。   
4.生成JSP前端代码（包括增、删、改、查）。

### 使用方法

1.把源码下到eclipse中。   
2.修改配置文件：/src/main/resources/generate.xml   
3.打开程序入口（main方法）：/org/mybatis/generator/api/ShellRunner.java。   
4.配置运行参数（configfile配置的是generate.xml文件位置）：-configfile D:\workspace\Demo\mybatis-generator-core\mybatis-generator-core\src\main\resources\generate.xml -overwrite

