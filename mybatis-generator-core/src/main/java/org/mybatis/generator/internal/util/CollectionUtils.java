package org.mybatis.generator.internal.util;

import java.util.Collection;


/**
 * 集合工具类
 * @author tangdelong
 * 2015年12月31日
 */
public class CollectionUtils {

	public static boolean isEmpty(Collection coll) {
		return (coll == null || coll.isEmpty());
	}
	
	public static boolean isNotEmpty(Collection coll) {
		return !CollectionUtils.isEmpty(coll);
	}

}
