/*
 *  Copyright 2008 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.mybatis.generator.internal;

import static org.mybatis.generator.internal.util.StringUtility.isTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.mybatis.generator.api.CommentGenerator;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.CompilationUnit;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.InnerClass;
import org.mybatis.generator.api.dom.java.InnerEnum;
import org.mybatis.generator.api.dom.java.JavaElement;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.MergeConstants;
import org.mybatis.generator.config.PropertyRegistry;

/**
 * 注释生成
 * 
 * @update 2015-04-29  tangdelong
 * @author Jeff Butler
 * 
 */
public class DefaultCommentGenerator implements CommentGenerator {

	private Properties properties;
	private boolean suppressDate;
	private boolean suppressAllComments;

	public DefaultCommentGenerator() {
		super();
		properties = new Properties();
		suppressDate = false;
		suppressAllComments = false;
	}

	public void addJavaFileComment(CompilationUnit compilationUnit) {
		// add no file level comments by default
		return;
	}

	/**
	 * Adds a suitable comment to warn users that the element was generated, and
	 * when it was generated.
	 */
	public void addComment(XmlElement xmlElement) {
		if (suppressAllComments) {
			return;
		}

	}

	public void addRootComment(XmlElement rootElement) {
		// add no document level comments by default
		return;
	}

	public void addConfigurationProperties(Properties properties) {
		this.properties.putAll(properties);

		suppressDate = isTrue(properties.getProperty(PropertyRegistry.COMMENT_GENERATOR_SUPPRESS_DATE));

		suppressAllComments = isTrue(properties.getProperty(PropertyRegistry.COMMENT_GENERATOR_SUPPRESS_ALL_COMMENTS));
	}

	/**
	 * This method adds the custom javadoc tag for. You may do nothing if you do
	 * not wish to include the Javadoc tag - however, if you do not include the
	 * Javadoc tag then the Java merge capability of the eclipse plugin will
	 * break.
	 * 
	 * @param javaElement
	 *            the java element
	 */
	protected void addJavadocTag(JavaElement javaElement, boolean markAsDoNotDelete) {
		StringBuilder sb = new StringBuilder();
		String s = getDateString();
		if (s != null) {
			sb.append(" * @date ");
			sb.append(s);
			javaElement.addJavaDocLine(sb.toString());
		}
	}

	/**
	 * This method returns a formated date string to include in the Javadoc tag
	 * and XML comments. You may return null if you do not want the date in
	 * these documentation elements.
	 * 
	 * @return a string representing the current timestamp, or null
	 */
	protected String getDateString() {
		if (suppressDate) {
			return null;
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date dt = new Date();
			String date = sdf.format(dt);
			return date;
		}
	}

	public void addClassComment(JavaElement innerClass, IntrospectedTable introspectedTable,String layer) {
		if (suppressAllComments) {
			return;
		}
		
		innerClass.addJavaDocLine("/**"); //$NON-NLS-1$
		// 获取数据库注释
		String comment = introspectedTable.getFullyQualifiedTable().getRemark();
		if(layer.equalsIgnoreCase("mode")){
			innerClass.addJavaDocLine(" * " + comment);
		}else if(layer.equalsIgnoreCase("mapper")){
			innerClass.addJavaDocLine(" * " + comment+"Mapper类");
		}else if(layer.equalsIgnoreCase("service")){
			innerClass.addJavaDocLine(" * " + comment+"Service类");
		}else if(layer.equalsIgnoreCase("serviceImpl")){
			innerClass.addJavaDocLine(" * " + comment+"Service实现类");
		}else if(layer.equalsIgnoreCase("condition")){
			innerClass.addJavaDocLine(" * " + comment+"Condition类");
		}else if(layer.equalsIgnoreCase("controller")){
			innerClass.addJavaDocLine(" * " + comment+"Controller类");
		}
		
		innerClass.addJavaDocLine(" * ");
		innerClass.addJavaDocLine(" * @author "+introspectedTable.getContext().getAuthorConfiguration().getName());
		addJavadocTag(innerClass, false);
		innerClass.addJavaDocLine(" */"); //$NON-NLS-1$

	}

	public void addEnumComment(InnerEnum innerEnum, IntrospectedTable introspectedTable) {
		if (suppressAllComments) {
			return;
		}

	}

	/**
	 * 生成属性注释
	 * 
	 * @author tangdelong
	 * @date
	 */
	public void addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
		if (suppressAllComments) {
			return;
		}

		if (introspectedColumn.getRemarks() != null && !introspectedColumn.getRemarks().equals("")) {
			field.addJavaDocLine("/**"); //$NON-NLS-1$
			// 获取数据库表注释
			String comment = introspectedColumn.getRemarks();
			field.addJavaDocLine(" * " + comment);
	
//			addJavadocTag(field, false);
	
			field.addJavaDocLine(" */"); //$NON-NLS-1$
		}
	}

	public void addFieldComment(Field field, IntrospectedTable introspectedTable) {

	}

	public void addGeneralMethodComment(Method method,
            IntrospectedTable introspectedTable){
		
	}
	
	public void addGeneralMethodComment(Method method, IntrospectedTable introspectedTable,String comment) {
		if (suppressAllComments) {
			return;
		}
		
		method.addJavaDocLine("/**"); //$NON-NLS-1$
		method.addJavaDocLine(" * " + comment);
		method.addJavaDocLine(" * ");
//		addJavadocTag(method, false);
//		method.addJavaDocLine(" * @author tangdelong ");
		method.addJavaDocLine(" */"); //$NON-NLS-1$
	}

	public void addGetterComment(Method method, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
		if (suppressAllComments) {
			return;
		}

		if (introspectedColumn.getRemarks() != null && !introspectedColumn.getRemarks().equals("")) {
			method.addJavaDocLine("/**"); //$NON-NLS-1$
			// 获取数据库注释
			String comment = introspectedColumn.getRemarks();
			method.addJavaDocLine(" * " + comment);
	
//			addJavadocTag(method, false);
	
			method.addJavaDocLine(" */"); //$NON-NLS-1$
		}
	}

	public void addSetterComment(Method method, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
		if (suppressAllComments) {
			return;
		}

		if (introspectedColumn.getRemarks() != null && !introspectedColumn.getRemarks().equals("")) {
			method.addJavaDocLine("/**"); //$NON-NLS-1$
			// 获取数据库注释
			String comment = introspectedColumn.getRemarks();
			method.addJavaDocLine(" * " + comment);
	
//			addJavadocTag(method, false);
	
			method.addJavaDocLine(" */"); //$NON-NLS-1$
		}
	}

	public void addClassComment(InnerClass innerClass, IntrospectedTable introspectedTable, boolean markAsDoNotDelete) {
		if (suppressAllComments) {
			return;
		}

	}
}
