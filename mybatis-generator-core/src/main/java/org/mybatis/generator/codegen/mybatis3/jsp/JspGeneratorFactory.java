package org.mybatis.generator.codegen.mybatis3.jsp;

import org.mybatis.generator.codegen.AbstractGenerator;
import org.mybatis.generator.codegen.AbstractJspGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.templet.JspAddGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.templet.JspDetailGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.templet.JspListGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.templet.JspUpdateGenerator;

/**
 * 	JSP构造工厂
 *
 * @author tangdelong
 * 2015年6月18日
 */
public class JspGeneratorFactory extends AbstractGenerator{
	

	public AbstractJspGenerator getJspGenerator(String product){
		AbstractJspGenerator abstractJspGenerator = null;
		if(product.equalsIgnoreCase("list")){
			abstractJspGenerator = new JspListGenerator(); 
		}else if(product.equalsIgnoreCase("add")){
			abstractJspGenerator = new JspAddGenerator(); 
		}else if(product.equalsIgnoreCase("update")){
			abstractJspGenerator = new JspUpdateGenerator(); 
		}else if(product.equalsIgnoreCase("detail")){
			abstractJspGenerator = new JspDetailGenerator(); 
		}
		initAbstractJspGenerator(abstractJspGenerator);
		return abstractJspGenerator;
	}
	
	
	private void initAbstractJspGenerator(AbstractGenerator abstractGenerator){
		abstractGenerator.setContext(context);
        abstractGenerator.setIntrospectedTable(introspectedTable);
        abstractGenerator.setProgressCallback(progressCallback);
        abstractGenerator.setWarnings(warnings);
	}
	
}
