package org.mybatis.generator.codegen.mybatis3.service.elements;

import java.util.Set;
import java.util.TreeSet;

import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;

/**
 * service层,根据条件进行匹配查询
 * @author tangdelong
 * 2015年12月11日
 */
public class FindListMethodOfService extends AbstractJavaServiceMethodGenerator {
	@Override
	public void addServiceElements(TopLevelClass topLevelClass) {
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
        Method method = new Method();

        // 定义返回类型为List
        FullyQualifiedJavaType returnListType = new FullyQualifiedJavaType("java.util.List");
        FullyQualifiedJavaType returnType = introspectedTable.getRules()
                .calculateAllFieldsClass();
        returnListType.addTypeArgument(returnType);
        
        method.setReturnType(returnListType);
        
        importedTypes.add(returnType);
        importedTypes.add(returnListType);
        
        
        method.setVisibility(JavaVisibility.PUBLIC);
        method.setName(introspectedTable.getListStatementId());

        method.addParameter(new Parameter(returnType, "condition")); //$NON-NLS-1$

        // 方法内容
		FullyQualifiedJavaType mapper = new FullyQualifiedJavaType(introspectedTable.getMyBatis3JavaMapperType());
		StringBuffer bodyLineSB = new StringBuffer();
		bodyLineSB.append("return ");
		bodyLineSB.append(mapper.getInjectName() + "." + introspectedTable.getListStatementId() + "(condition);");
		method.addBodyLine(bodyLineSB.toString());
        
        
        context.getCommentGenerator().addGeneralMethodComment(method,
                introspectedTable,"根据查询条件进行匹配查询");

        
        if (context.getPlugins().clientInsertSelectiveMethodGenerated(
                method, topLevelClass, introspectedTable)) {
        	topLevelClass.addImportedTypes(importedTypes);
        	topLevelClass.addMethod(method);
        }
		
	}
}
