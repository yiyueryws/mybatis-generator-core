package org.mybatis.generator.codegen;

import org.mybatis.generator.api.dom.jsp.JspDocument;

public abstract class AbstractJspGenerator extends AbstractGenerator {
	 public abstract JspDocument getJspDocument();
}
