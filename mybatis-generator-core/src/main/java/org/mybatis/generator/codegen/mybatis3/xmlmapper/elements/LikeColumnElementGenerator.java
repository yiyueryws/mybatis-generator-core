package org.mybatis.generator.codegen.mybatis3.xmlmapper.elements;


import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.MyBatis3FormattingUtilities;

/**
 * 生成模糊查询字段xml
 * 
 * @author tangdelong
 * 2015年12月1日
 */
public class LikeColumnElementGenerator extends AbstractXmlElementGenerator{

	@Override
	public void addElements(XmlElement parentElement) {
		XmlElement answer = new XmlElement("sql"); 

        answer.addAttribute(new Attribute("id", "like")); 
        
        context.getCommentGenerator().addComment(answer);
        

        StringBuilder sb = new StringBuilder();
        
        for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
        	 XmlElement insertNotNullElement = new XmlElement("if"); //$NON-NLS-1$
             sb.setLength(0);
             sb.append(introspectedColumn.getJavaProperty());
             sb.append(" != null");
             
             boolean flag = false;
             // 特殊类型处理
             if("java.lang.String".equals(introspectedColumn.getFullyQualifiedJavaType().toString())){
            	 flag = true;
             }
             
             if(flag){
             	sb.append(" and ");
 	           	sb.append(introspectedColumn.getJavaProperty());
 	           	sb.append(" != '' ");
             }
             
             insertNotNullElement.addAttribute(new Attribute("test", sb.toString())); 

             sb.setLength(0);
             sb.append(" AND ");
             sb.append(MyBatis3FormattingUtilities
                     .getEscapedColumnName(introspectedColumn));
             
//           sb.append(" like \'%"); 
//           sb.append("${"+introspectedColumn.getJavaProperty(null)+"}");
//           sb.append("%\'"); 
           
	         sb.append(" like CONCAT('");
	         sb.append("%',");
	         sb.append("#{");
	         sb.append(introspectedColumn.getJavaProperty(null));
	         sb.append("},");
	         sb.append("'%')");
             
             
             
             insertNotNullElement.addElement(new TextElement(sb.toString()));
             
             answer.addElement(insertNotNullElement);

        }

        if (context.getPlugins().sqlMapSelectByPrimaryKeyElementGenerated(answer,introspectedTable)) {
            parentElement.addElement(answer);
        }

		
	}

}
