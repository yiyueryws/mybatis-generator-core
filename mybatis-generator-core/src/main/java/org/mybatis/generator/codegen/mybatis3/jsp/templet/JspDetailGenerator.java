package org.mybatis.generator.codegen.mybatis3.jsp.templet;

import java.lang.reflect.Constructor;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.OutputUtilities;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.jsp.JspAttribute;
import org.mybatis.generator.api.dom.jsp.JspDocument;
import org.mybatis.generator.api.dom.jsp.JspElement;
import org.mybatis.generator.api.dom.jsp.JspTextElement;
import org.mybatis.generator.codegen.AbstractJspGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.utils.JspUtils;

/**
 * JSP详情页
 *
 * @author tangdelong
 * 2015年6月23日
 */
public class JspDetailGenerator extends AbstractJspGenerator {

	@Override
	public JspDocument getJspDocument() {
		List<String> strLs = new ArrayList<String>();
		strLs.add(JspUtils.getInclude1());
		strLs.add("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
		
		JspDocument document = new JspDocument(strLs);
        document.setRootElement(getJspDetailElement());


        return document;
	}
	
	protected JspElement getJspDetailElement(){
		FullyQualifiedJavaType model = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
		
		JspElement html = new JspElement("html");
		
		// head - start
		JspElement head = new JspElement("head");
		html.addJspElements(head);
		head.addJspElements(new JspTextElement(JspUtils.getInclude2()));
		JspElement meta = new JspElement("meta");
		head.addJspElements(meta);
		meta.addJspAttributes(new JspAttribute("http-equiv", "Content-Type"));
		meta.addJspAttributes(new JspAttribute("content", "text/html; charset=UTF-8"));
		
		
		JspElement title = new JspElement("title");
		head.addJspElements(title);
		title.addJspElements(new JspTextElement(introspectedTable.getFullyQualifiedTable().getRemark().trim()+"详情"));
		
		// head - end
		
		
		// body -start
		JspElement body = new JspElement("body");
		html.addJspElements(body);
		JspElement div1 =  new JspElement("div");
		body.addJspElements(div1);
		div1.addJspAttributes(new JspAttribute("class", "pad_10"));
		
		JspElement div2 =  new JspElement("div");
		div1.addJspElements(div2);
		div2.addJspAttributes(new JspAttribute("class", "common-form"));
		
		JspElement fieldset = new JspElement("fieldset");
		div2.addJspElements(fieldset);
		JspElement legend = new JspElement("legend");
		fieldset.addJspElements(legend);
		legend.addJspElements(new JspTextElement(introspectedTable.getFullyQualifiedTable().getRemark().trim()+"详情"));
		JspElement table = new JspElement("table");
		fieldset.addJspElements(table);
		table.addJspAttributes(new JspAttribute("width", "100%"));
		table.addJspAttributes(new JspAttribute("class", "table_form"));
		
		JspElement tbody = new JspElement("tbody");
		table.addJspElements(tbody);
		
		int columnsSize = introspectedTable.getAllColumns().size();
		for (int i=0;i<columnsSize;i++) {
			IntrospectedColumn introspectedColumn = introspectedTable.getAllColumns().get(i);
			JspElement tr = new JspElement("tr");
			tbody.addJspElements(tr);
			JspElement th = new JspElement("th");
			tr.addJspElements(th);
			th.addJspElements(new JspTextElement(introspectedColumn.getRemarks()+":"));
			JspElement td = new JspElement("td");
			tr.addJspElements(td);
			JspElement input = new JspElement("input");
			td.addJspElements(input);
			input.addJspAttributes(new JspAttribute("name", introspectedColumn.getJavaProperty()));
			input.addJspAttributes(new JspAttribute("type", "text"));
			input.addJspAttributes(new JspAttribute("class", "input-text"));
			
			String typeStr = introspectedColumn.getFullyQualifiedJavaType().getBaseQualifiedName();
			if(typeStr.equals("java.sql.Timestamp")){
				input.addJspAttributes(new JspAttribute("value", "<fmt:formatDate value=\"${"+model.getInjectName()+"."+introspectedColumn.getJavaProperty()+"}\" pattern=\"yyyy-MM-dd\"/>"));
			}else if(typeStr.equals("java.util.Date")){
				input.addJspAttributes(new JspAttribute("value", "<fmt:formatDate value=\"${"+model.getInjectName()+"."+introspectedColumn.getJavaProperty()+"}\" pattern=\"yyyy-MM-dd\"/>"));
			}else{
				input.addJspAttributes(new JspAttribute("value", "${"+model.getInjectName()+"."+introspectedColumn.getJavaProperty()+"}"));
			}
	
			
			
			// 一行显示2个列字段
			if(++i< columnsSize){
				IntrospectedColumn introspectedColumn2 = introspectedTable.getAllColumns().get(i);
				JspElement th2 = new JspElement("th");
				tr.addJspElements(th2);
				th2.addJspElements(new JspTextElement(introspectedColumn2.getRemarks()+":"));
				JspElement td2 = new JspElement("td");
				tr.addJspElements(td2);
				JspElement input2 = new JspElement("input");
				td2.addJspElements(input2);
				input2.addJspAttributes(new JspAttribute("name", introspectedColumn2.getJavaProperty()));
				input2.addJspAttributes(new JspAttribute("type", "text"));
				input2.addJspAttributes(new JspAttribute("class", "input-text"));
				
				String typeStr2 = introspectedColumn2.getFullyQualifiedJavaType().getBaseQualifiedName();
				if(typeStr2.equals("java.sql.Timestamp")){
					input2.addJspAttributes(new JspAttribute("value", "<fmt:formatDate value=\"${"+model.getInjectName()+"."+introspectedColumn2.getJavaProperty()+"}\" pattern=\"yyyy-MM-dd\"/>"));
				}else if(typeStr2.equals("java.util.Date")){
					input2.addJspAttributes(new JspAttribute("value", "<fmt:formatDate value=\"${"+model.getInjectName()+"."+introspectedColumn2.getJavaProperty()+"}\" pattern=\"yyyy-MM-dd\"/>"));
				}else{
					input2.addJspAttributes(new JspAttribute("value", "${"+model.getInjectName()+"."+introspectedColumn2.getJavaProperty()+"}"));
				}
			}
		}
		
		JspElement div3 = new JspElement("div");
		div2.addJspElements(div3);
		div3.addJspAttributes(new JspAttribute("class", "bk15"));
		
		
		JspElement button2 = new JspElement("button");
		div2.addJspElements(button2);
		button2.addJspAttributes(new JspAttribute("class", "self-btn gray"));
		button2.addJspAttributes(new JspAttribute("type", "button"));
		button2.addJspAttributes(new JspAttribute("onclick", "goBack()"));
		button2.addJspElements(new JspTextElement("返回"));
		
		// 添加form - 结束
		// body -end
		
		// script - start
		JspElement script = new JspElement("script");
		html.addJspElements(script);
		script.addJspAttributes(new JspAttribute("type","text/javascript"));
		
		JspTextElement goBack = new JspTextElement();
		StringBuilder goBackStr = new StringBuilder();
		goBackStr.append("\nfunction goBack(){\n");
		OutputUtilities.jspIndent(goBackStr, 1);
		goBackStr.append("location.href = \"<c:url value=\'"+model.getInjectName()+"/"+model.getInjectName()+"Search.do\' />\";");
		OutputUtilities.jspIndent(goBackStr, 1);
		goBackStr.append("\n}\n\n");
		goBack.setContent(goBackStr.toString());
		script.addJspElements(goBack);
		
		// script - end
		
		
		return html;
	}

}
