package org.mybatis.generator.codegen.mybatis3.jsp.templet;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.OutputUtilities;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.jsp.JspAttribute;
import org.mybatis.generator.api.dom.jsp.JspDocument;
import org.mybatis.generator.api.dom.jsp.JspElement;
import org.mybatis.generator.api.dom.jsp.JspTextElement;
import org.mybatis.generator.codegen.AbstractJspGenerator;
import org.mybatis.generator.codegen.mybatis3.jsp.utils.JspUtils;
import org.mybatis.generator.internal.util.CollectionUtils;

/**
 * jspList页生成
 *
 * @author tangdelong
 * 2015年6月17日
 */
public class JspListGenerator extends AbstractJspGenerator{

	@Override
	public JspDocument getJspDocument() {
		List<String> strLs = new ArrayList<String>();
		strLs.add(JspUtils.getInclude1());
		strLs.add("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
		
		JspDocument document = new JspDocument(strLs);
        document.setRootElement(getJspListElement());


        return document;
	}
	
	
	protected JspElement getJspListElement(){
		FullyQualifiedJavaType model = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
		
		JspElement html = new JspElement("html");
		
		// head - start
		JspElement head = new JspElement("head");
		html.addJspElements(head);
		head.addJspElements(new JspTextElement(JspUtils.getInclude2()));
		JspElement meta = new JspElement("meta");
		head.addJspElements(meta);
		meta.addJspAttributes(new JspAttribute("http-equiv", "Content-Type"));
		meta.addJspAttributes(new JspAttribute("content", "text/html; charset=UTF-8"));
		
		
		JspElement title = new JspElement("title");
		head.addJspElements(title);
		title.addJspElements(new JspTextElement(introspectedTable.getFullyQualifiedTable().getRemark().trim()+"查询"));
		
		// head - end
		
		// body -start
		JspElement body = new JspElement("body");
		html.addJspElements(body);
		JspElement div1 =  new JspElement("div");
		body.addJspElements(div1);
		div1.addJspAttributes(new JspAttribute("class", "table-list pad-lr-10"));
		// 查询条件-开始
		JspElement form = new JspElement("form");
		div1.addJspElements(form);
		form.addJspAttributes(new JspAttribute("id", model.getInjectName()+"Searchform"));
		form.addJspAttributes(new JspAttribute("action", "<c:url value=\""+model.getInjectName()+"/"+model.getInjectName()+"Search.do\" />"));
		form.addJspAttributes(new JspAttribute("method", "get"));
		
		
		JspElement table = new JspElement("table");
		form.addJspElements(table);
		table.addJspAttributes(new JspAttribute("width", "100%"));
		table.addJspAttributes(new JspAttribute("cellspacing", "0"));
		table.addJspAttributes(new JspAttribute("class", "search-form"));
		
		
		
		JspElement tbody = new JspElement("tbody");
		table.addJspElements(tbody);
		JspElement tr = new JspElement("tr");
		tbody.addJspElements(tr);
		JspElement td = new JspElement("td");
		tr.addJspElements(td);
		JspElement div2 = new JspElement("div");
		div2.addJspAttributes(new JspAttribute("class", "explain-col"));
		td.addJspElements(div2);
		
		JspTextElement div2Text = new JspTextElement();
		div2.addJspElements(div2Text);
		
		int i=1;
		StringBuilder text = new StringBuilder();
		for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
			text.append("\n");
			OutputUtilities.jspIndent(text, 8);
			text.append(introspectedColumn.getRemarks().trim()+":");
			if(i%5 == 0){
				text.append("<input name="+introspectedColumn.getJavaProperty()+" type=\"text\" value=\"${param."+introspectedColumn.getJavaProperty()+" }\"/><br/> ");
			}else{
				text.append("<input name="+introspectedColumn.getJavaProperty()+" type=\"text\" value=\"${param."+introspectedColumn.getJavaProperty()+" }\"/> ");
			}
			i++;
		}
		text.append("\n");
		OutputUtilities.jspIndent(text, 8);
		text.append("<button type=\"submit\" class=\"self-btn gray\">搜索</button>");
		text.append("\n");
		OutputUtilities.jspIndent(text, 8);
		text.append("<button type=\"reset\" class=\"self-btn gray\">重置</button>");
		text.append("\n");
		OutputUtilities.jspIndent(text, 7);
		div2Text.setContent(text.toString());
		
		
		
		// 查询条件-结束
		
		
		
		// 数据显示 - 开始
		JspElement table1 = new JspElement("table");
		div1.addJspElements(table1);
		table1.addJspAttributes(new JspAttribute("width", "100%"));
		table1.addJspAttributes(new JspAttribute("cellspacing", "0"));
		
		JspElement thead1 = new JspElement("thead");
		table1.addJspElements(thead1);
		JspElement tr1 = new JspElement("tr");
		thead1.addJspElements(tr1);
		
		JspElement th1 = new JspElement("th");
		th1.addJspAttributes(new JspAttribute("width", "60"));
		th1.addJspAttributes(new JspAttribute("align", "left"));
		th1.addJspElements(new JspTextElement("序号"));
		tr1.addJspElements(th1);
		for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
			JspElement th2 = new JspElement("th");
			th2.addJspElements(new JspTextElement(introspectedColumn.getRemarks().trim()));
			tr1.addJspElements(th2);
		}
		
		JspElement tbody1 = new JspElement("tbody");
		table1.addJspElements(tbody1);
		JspElement forEach = new JspElement("c:forEach");
		table1.addJspElements(forEach);
		forEach.addJspAttributes(new JspAttribute("var", "item"));
		forEach.addJspAttributes(new JspAttribute("items", "${page.list}"));
		forEach.addJspAttributes(new JspAttribute("varStatus", "x"));
		
		
		JspElement tr2 = new JspElement("tr");
		forEach.addJspElements(tr2);
		JspElement td2 = new JspElement("td");
		tr2.addJspElements(td2);
		td2.addJspAttributes(new JspAttribute("align","left"));
		td2.addJspAttributes(new JspAttribute("width","60"));
		td2.addJspElements(new JspTextElement("${x.index+1 }"));
		
		for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
			JspElement td3 = new JspElement("td");
			tr2.addJspElements(td3);
			td3.addJspAttributes(new JspAttribute("align", "center"));
			td3.addJspElements(new JspTextElement("${item."+introspectedColumn.getJavaProperty()+" }"));
			FullyQualifiedJavaType type = introspectedColumn.getFullyQualifiedJavaType();
			
		}
		
		JspElement td4 = new JspElement("td");
		tr2.addJspElements(td4);
		td4.addJspAttributes(new JspAttribute("align", "center"));
		
		// 判断是否有主键
		if(CollectionUtils.isNotEmpty(introspectedTable.getPrimaryKeyColumns())){
			JspElement button1 = new JspElement("button");
			button1.addJspAttributes(new JspAttribute("type", "button"));
			button1.addJspAttributes(new JspAttribute("class", "self-btn gray"));
			button1.addJspAttributes(new JspAttribute("onclick", "detail(${item."+introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty()+"})"));
			button1.addJspElements(new JspTextElement("详情"));
			td4.addJspElements(button1);
			
			JspElement button2 = new JspElement("button");
			button2.addJspAttributes(new JspAttribute("type", "button"));
			button2.addJspAttributes(new JspAttribute("class", "self-btn gray"));
			button2.addJspAttributes(new JspAttribute("onclick", "update(${item."+introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty()+"})"));
			button2.addJspElements(new JspTextElement("修改"));
			td4.addJspElements(button2);
			
			JspElement button3 = new JspElement("button");
			button3.addJspAttributes(new JspAttribute("type", "button"));
			button3.addJspAttributes(new JspAttribute("class", "self-btn gray"));
			button3.addJspAttributes(new JspAttribute("onclick", "del(${item."+introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty()+"})"));
			button3.addJspElements(new JspTextElement("删除"));
			td4.addJspElements(button3);
		}
		
		// 数据显示 - 结束
		
		// 数据分页 - 开始
		JspElement div3 = new JspElement("div");
		div1.addJspElements(div3);
		div3.addJspAttributes(new JspAttribute("id", "pages"));
		div3.addJspElements(new JspTextElement("${page.pagination }"));
		// 数据分页 - 结束
		
		
		// body -end
		
		
		// script - start
		
		JspElement script = new JspElement("script");
		html.addJspElements(script);
		script.addJspAttributes(new JspAttribute("type","text/javascript"));
		
		// 删除方法
		JspTextElement delete = new JspTextElement();
		script.addJspElements(delete);
		StringBuilder delStr = new StringBuilder();
		delStr.append("\nfunction del(id){\n");
		OutputUtilities.jspIndent(delStr, 1);
		delStr.append("if(confirm(\"确定删除本条记录？\")){\n");
		OutputUtilities.jspIndent(delStr, 2);
		delStr.append("location.href = \"<c:url value=\'"+model.getInjectName()+"/"+model.getInjectName()+"Delete.do\' />?id=\"+id;\n");
		OutputUtilities.jspIndent(delStr, 1);
		delStr.append("}\n}\n\n");
		delete.setContent(delStr.toString());
		
		// 详情方法
		JspTextElement detail = new JspTextElement();
		script.addJspElements(detail);
		StringBuilder detailStr = new StringBuilder();
		detailStr.append("\nfunction del(id){\n");
		OutputUtilities.jspIndent(detailStr, 1);
		detailStr.append("location.href = \"<c:url value=\'"+model.getInjectName()+"/"+model.getInjectName()+"Detail.do\' />?id=\"+id;");
		OutputUtilities.jspIndent(detailStr, 1);
		detailStr.append("\n}\n\n");
		detail.setContent(detailStr.toString());
				
		// 修改方法
		JspTextElement update = new JspTextElement();
		script.addJspElements(update);
		StringBuilder updateStr = new StringBuilder();
		updateStr.append("\nfunction del(id){\n");
		OutputUtilities.jspIndent(updateStr, 1);
		updateStr.append("location.href = \"<c:url value=\'"+model.getInjectName()+"/"+model.getInjectName()+"Update.do\' />?id=\"+id;");
		OutputUtilities.jspIndent(updateStr, 1);
		updateStr.append("\n}\n\n");
		update.setContent(updateStr.toString());
		
		// script - end
		
		
		return html;
	}

}
