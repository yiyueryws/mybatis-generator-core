package org.mybatis.generator.codegen.mybatis3.controller.elements;

import java.util.Set;
import java.util.TreeSet;

import org.mybatis.generator.api.dom.OutputUtilities;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.util.StringUtility;

/**
 * controller 查询方法
 *
 * @author tangdelong
 * 2015年5月25日
 */
public class SearchMethodControllerGenerator extends AbstractJavaControllerMethodGenerator {

	@Override
	public void addControllerElements(TopLevelClass topLevelClass) {
		FullyQualifiedJavaType model = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
		
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
		Method method = new Method();
		method.setVisibility(JavaVisibility.PUBLIC);
		method.setReturnType(new FullyQualifiedJavaType("org.springframework.web.servlet.ModelAndView"));
		method.setName(model.getInjectName()+"Search");

		
		method.addAnnotation("@RequestMapping(\"/"+model.getInjectName()+"Search\")");
		
		FullyQualifiedJavaType request = new FullyQualifiedJavaType("javax.servlet.http.HttpServletRequest");
		method.addParameter(new Parameter(request, "request"));
		FullyQualifiedJavaType page = new FullyQualifiedJavaType("int");
		method.addParameter(new Parameter(page, "page","\n\t\t\t@RequestParam(required=false, defaultValue=\"1\")"));
		FullyQualifiedJavaType rows = new FullyQualifiedJavaType("int");
		method.addParameter(new Parameter(rows, "rows","\n\t\t\t@RequestParam(required=false, defaultValue=\"10\")"));
		FullyQualifiedJavaType param = new FullyQualifiedJavaType(introspectedTable.getBaseRecordConditionType());
		method.addParameter(new Parameter(param, "param","\n\t\t\t"));
		
		
		// 方法内容
		FullyQualifiedJavaType service = new FullyQualifiedJavaType(introspectedTable.getMyBatis3JavaServiceType());
		
		String jspTarget = "";
		if(context.getJavaJSPGeneratorConfiguration() != null){
			jspTarget = context.getJavaJSPGeneratorConfiguration().getTargetPackage();
		}
		method.addBodyLine("ModelAndView view = new ModelAndView(\"/"+jspTarget+"/"+introspectedTable.getFullyQualifiedTable().getDomainObjectName().toLowerCase()+"/"+OutputUtilities.removeSuffix(introspectedTable.getMyBatis3JSPListFileName())+"\");");
		method.addBodyLine("");
		method.addBodyLine("PageHelper.startPage(page, rows);");
		method.addBodyLine("List<"+model.getShortName()+"> "+model.getInjectName()+"List = "+service.getInjectName() + "." + introspectedTable.getLikeStatementId() + "(param);");
//		method.addBodyLine("PageInfo pageinfo = new PageInfo("+model.getInjectName()+"List, request);");
		method.addBodyLine("");
//		method.addBodyLine("view.addObject(\"page\", pageinfo);");
		method.addBodyLine("view.addObject(\""+model.getInjectName()+"List\","+model.getInjectName()+"List);");
		method.addBodyLine("");
		method.addBodyLine("return view;");

		context.getCommentGenerator().addGeneralMethodComment(method, introspectedTable, "根据条件查询结果集");

		if (context.getPlugins().clientDeleteByPrimaryKeyMethodGenerated(method, topLevelClass, introspectedTable)) {
			topLevelClass.addImportedTypes(importedTypes);
			topLevelClass.addMethod(method);
		}

	}

}
