package org.mybatis.generator.api;

import org.mybatis.generator.api.dom.jsp.JspDocument;

/**
 * 生成jsp文件类
 *
 * @author tangdelong 2015年6月17日
 */
public class GeneratedJspFile extends GeneratedFile {

	private JspDocument jspDocument;

	private String fileName;

	private String targetPackage;

	private boolean isMergeable;

	private JspFormatter jspFormatter;
	
	

	 /**
     * 
     * @param document
     * @param fileName
     * @param targetPackage
     * @param targetProject
     * @param isMergeable
     *            true if the file can be merged by the built in XML file
     *            merger.
     */
    public GeneratedJspFile(JspDocument jspDocument, String fileName,
            String targetPackage, String targetProject) {
        super(targetProject);
        this.jspDocument = jspDocument;
        this.fileName = fileName;
        this.targetPackage = targetPackage;
        this.jspFormatter = jspFormatter;
    }
    
    @Override
    public String getFormattedContent() {
        return jspDocument.getFormattedContent();
    }





	public String getFileName() {
		return fileName;
	}



	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	public String getTargetPackage() {
		return targetPackage;
	}



	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}



	public boolean isMergeable() {
		return isMergeable;
	}



	public void setMergeable(boolean isMergeable) {
		this.isMergeable = isMergeable;
	}

	public JspFormatter getJspFormatter() {
		return jspFormatter;
	}

	public void setJspFormatter(JspFormatter jspFormatter) {
		this.jspFormatter = jspFormatter;
	}





}
