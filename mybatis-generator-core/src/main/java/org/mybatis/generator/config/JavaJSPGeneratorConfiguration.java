package org.mybatis.generator.config;

/**
 * jsp生成配置文件
 *
 * @author tangdelong
 * 2015年5月24日
 */
public class JavaJSPGeneratorConfiguration extends PropertyHolder {
	
	private String targetPackage;

    private String targetProject;

	public String getTargetPackage() {
		return targetPackage;
	}

	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}

	public String getTargetProject() {
		return targetProject;
	}

	public void setTargetProject(String targetProject) {
		this.targetProject = targetProject;
	}
}

