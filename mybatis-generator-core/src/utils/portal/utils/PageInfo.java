package portal.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


public class PageInfo extends com.github.pagehelper.PageInfo {
	
	private HttpServletRequest request;
	private String uri = null;
	
	public PageInfo(){
		super(null);
	}

	public PageInfo(List list, HttpServletRequest request) {
		super(list);
		this.request = request;
		this.uri = this.request.getRequestURI();
	}
	
	public String getPagination() {
		StringBuffer multipage = new StringBuffer();
		Long num = super.getTotal();
		int curr_page = Math.max(super.getPageNum(), 1);
		int perpage = super.getPageSize();
		
		int setpages = 10, page;
		int offset, pages;
		int from, to;
		int more;
		
		String urlrule = url_par();
		
		if(num > perpage) {
			page = setpages+1;
			offset = (int) Math.ceil(setpages/2-1);
			pages = this.getPages();//(int) Math.ceil(num / perpage);
			from = (int)(curr_page - offset);
			to = (int)(curr_page + offset);
			more = 0;
			if(page >= pages) {
				from = 2;
				to = pages-1;
			} else {
				if(from <= 1) {
					to = page-1;
					from = 2;
				}  else if(to >= pages) {
					from = pages-(page-2);
					to = pages-1;
				}
				more = 1;
			}
			multipage.append("<a class=\"a1\">").append(num).append("条</a>");
			if(curr_page>0) {
				multipage.append(" <a href=\"").append(pageurl(urlrule, curr_page-1)).append("\" class=\"a1\">上一页</a>");
				if(curr_page==1) {
					multipage.append(" <span>1</span>");
				} else if(curr_page >6 && more!=0) {
					multipage.append(" <a href=\"").append(pageurl(urlrule, 1)).append("\">1</a>..");
				} else {
					multipage.append(" <a href=\"").append(pageurl(urlrule, 1)).append("\">1</a>");
				}
			}
			for(int i = from; i <= to; i++) {
				if(i != curr_page) {
					multipage.append(" <a href=\"").append(pageurl(urlrule, i)).append("\">").append(i).append("</a>");
				} else {
					multipage.append(" <span>").append(i).append("</span>");
				}
			}
			if(curr_page<pages) {
				if(curr_page<pages-5 && more!=0) {
					multipage.append(" ..<a href=\"").append(pageurl(urlrule, pages)).append("\">").append(pages).append("</a> <a href=\"").append(pageurl(urlrule, curr_page+1)).append("\" class=\"a1\">下一页</a>");
				} else {
					multipage.append(" <a href=\"").append(pageurl(urlrule, pages)).append("\">").append(pages).append("</a> <a href=\"").append(pageurl(urlrule, curr_page+1)).append("\" class=\"a1\">下一页</a>");
				}
			} else if(curr_page==pages) {
				multipage.append(" <span>").append(pages).append("</span> <a href=\"").append(pageurl(urlrule, curr_page)).append("\" class=\"a1\">下一页</a>");
			} else {
				multipage.append(" <a href=\"").append(pageurl(urlrule, pages)).append("\">").append(pages).append("</a> <a href=\"").append(pageurl(urlrule, curr_page+1)).append("\" class=\"a1\">下一页</a>");
			}
		}
		
		return multipage.toString();
	}
	
	private String url_par() {
		StringBuffer buf = new StringBuffer();
		
		buf.append(this.uri).append("?");
		
		Map params = this.request.getParameterMap();
		Iterator<String> it = params.keySet().iterator();
		while(it.hasNext()) {
			String key = it.next();
			if(!"page".equals(key)) buf.append(key).append("=").append(this.request.getParameter(key)).append("&");
		}
		
		return buf.toString();
	}
	
	private String pageurl(String urlrule, int page) {
		return urlrule + "page="+page;
	}

	


}
